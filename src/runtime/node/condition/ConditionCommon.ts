import { Condition, NodeStatus } from "../index";
import { btclass } from "../../core/decorator";

@btclass("ConditionCommon")
export default class ConditionCommon extends Condition {}
