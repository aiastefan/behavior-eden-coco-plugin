import { Action, NodeStatus } from "../index";
import { btclass } from "../../core/decorator";

@btclass("ActionCommon")
export class ActionCommon extends Action {}
