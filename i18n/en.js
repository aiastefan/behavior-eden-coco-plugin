"use strict";
module.exports = {
  open_panel: "editor",
  send_to_panel: "Send message to Default Panel",
  description: "Behavior Eden is an open-source Cocos Creator lightweight visual behavior tree editor",
};
